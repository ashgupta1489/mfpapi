/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package com.ibm.mfpstartercordova;

public final class R {
  public static final class drawable {
    public static final int screen=0x7f010000;
    public static final int splash=0x7f010001;
  }
  public static final class mipmap {
    public static final int icon=0x7f020000;
  }
  public static final class string {
    public static final int activity_name=0x7f030000;
    public static final int app_name=0x7f030001;
    public static final int cancel=0x7f030002;
    public static final int close=0x7f030003;
    public static final int directUpdateDownloadingMessage=0x7f030004;
    public static final int downloadingWebResourcesMessage=0x7f030005;
    public static final int error=0x7f030006;
    public static final int kb=0x7f030007;
    public static final int launcher_name=0x7f030008;
    public static final int unpackingWebResourcesMessage=0x7f030009;
    public static final int wlAppIdTitle=0x7f03000a;
    public static final int wlAppVersionTitle=0x7f03000b;
    public static final int wlWebResourcesCategory=0x7f03000c;
    public static final int worklightInitInternalError=0x7f03000d;
    public static final int worklightInitNotEnoughSpace=0x7f03000e;
  }
  public static final class xml {
    public static final int config=0x7f040000;
  }
}